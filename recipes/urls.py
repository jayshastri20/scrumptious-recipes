from django.urls import path
from recipes.views import show_recipe

urlpatterns = [
    path("1/", show_recipe),
]
